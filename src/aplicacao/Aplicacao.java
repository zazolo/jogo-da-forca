package aplicacao;

import fabrica.ElementoGraficoFactory;
//import fabrica.ElementoGraficoImagemFactory;
import fabrica.ElementoGraficoTextoFactory;
import fabrica.JogadorFactory;
import fabrica.JogadorFactoryImpl;
import fabrica.PalavraFactory;
import fabrica.PalavraFactoryImpl;
import fabrica.RodadaFactory;
import fabrica.RodadaSorteioFactory;
import fabrica.TemaFactory;
import fabrica.TemaFactoryImpl;
//import repositorio.BDRRepositoryFactory;
import repositorio.MemoriaRepositoryFactory;
import repositorio.RepositoryFactory;

public class Aplicacao
{
	private final static String[] TIPOS_REPOSITORY_FACTORY = {"memoria", "relacional"};
	private final static String[] TIPOS_ELEMENTO_GRAFICO_FACTORY = {"texto", "imagem"};
	private final static String[] TIPOS_RODADA_FACTORY = {"sorteio"};
	private static Aplicacao soleInstance = new Aplicacao();
	private String tipoRepositoryFacotory;
	private String tipoElementoGraficoFactory;
	private String tipoRodadaFactory;

	public static Aplicacao getSoleInstance()
	{
		return soleInstance;
	}
	
	private Aplicacao()
	{
		
	}
	
	public void setup()
	{
		this.setTipoElementoGraficoFactory(TIPOS_ELEMENTO_GRAFICO_FACTORY[0]);
		this.setTipoRepositoryFactory(TIPOS_REPOSITORY_FACTORY[0]);
		this.setTipoRodadaFactory(TIPOS_RODADA_FACTORY[0]);
	}
	
	public String[] getTiposRepositoryFactory()
	{
		return TIPOS_REPOSITORY_FACTORY;
	}
	
	public void setTipoRepositoryFactory(String tipo)
	{
		this.tipoRepositoryFacotory = tipo;
	}
	
	public RepositoryFactory getRepositoryFactory()
	{
		if(TIPOS_REPOSITORY_FACTORY[0] == this.tipoRepositoryFacotory)
			return MemoriaRepositoryFactory.getSoleInstance();
		
//		if(TIPOS_REPOSITORY_FACTORY[1] == this.tipoRepositoryFacotory)
//			return BDRRepositoryFactory.getSoleInstance();
		
		return null;
	}
	
	public String[] getTiposElementoGraficoFactory()
	{
		return TIPOS_ELEMENTO_GRAFICO_FACTORY;
	}
	
	public void setTipoElementoGraficoFactory(String tipo)
	{
		this.tipoElementoGraficoFactory = tipo;
	}
	
	public ElementoGraficoFactory getElementoGraficoFactory()
	{
		if(TIPOS_ELEMENTO_GRAFICO_FACTORY[0] == this.tipoElementoGraficoFactory)
			return ElementoGraficoTextoFactory.getSoleInstance();
		
//		if(TIPOS_ELEMENTO_GRAFICO_FACTORY[1] == this.tipoElementoGraficoFactory)
//			return ElementoGraficoImagemFactory.getSoleInstance();
		
		return null;
	}
	
	public static String[] getTiposRodadaFactory()
	{
		return TIPOS_RODADA_FACTORY;
	}
	
	public void setTipoRodadaFactory(String tipo)
	{
		this.tipoRodadaFactory = tipo;
	}
	
	public RodadaFactory getRodadaFactory()
	{
		if(TIPOS_RODADA_FACTORY[0] == this.tipoRodadaFactory)
		{
			RepositoryFactory repositoryFactory = this.getRepositoryFactory();
			RodadaSorteioFactory.createSoleInstance(repositoryFactory.getRodadaRepository(), repositoryFactory.getRepoTemaRepository(), repositoryFactory.getPalavraRepository());
			return RodadaSorteioFactory.getSoleInstance();
		}
		
		return null;
	}
	
	public TemaFactory getTemaFactory()
	{
		TemaFactoryImpl.createSoleInstance(this.getRepositoryFactory().getRepoTemaRepository());
		return TemaFactoryImpl.getSoleInstance();
	}
	
	public PalavraFactory getPalavraFactory()
	{
		PalavraFactoryImpl.createSoleInstance(this.getRepositoryFactory().getPalavraRepository());
		return PalavraFactoryImpl.getSoleInstance();
	}
	
	public JogadorFactory getJogadorFactory()
	{
		JogadorFactoryImpl.createSoleInstance(this.getRepositoryFactory().getJogadorRepository());
		return JogadorFactoryImpl.getSoleInstance();
	}
}
