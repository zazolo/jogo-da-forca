package dominio;

public final class BonecoTexto implements Boneco
{
	private static BonecoTexto soleInstance = new BonecoTexto();
	
	
	public static BonecoTexto getSoleInstance()
	{
		return soleInstance;
	}
	
	private BonecoTexto()
	{
		
	}

	public void exibir(Object contexto, int partes)
	{
		String[] partesTXTs = new String[11];

		partesTXTs[0] = "";
		partesTXTs[1] = "cabe�a";
		partesTXTs[2] = "cabe�a, olho esquerdo";
		partesTXTs[3] = "cabe�a, olho esquerdo, olho direito ";
		partesTXTs[4] = "cabe�a, olho esquerdo, olho direito, nariz ";
		partesTXTs[5] = "cabe�a, olho esquerdo, olho direito, nariz, boca ";
		partesTXTs[6] = "cabe�a, olho esquerdo, olho direito, nariz, boca, tronco ";
		partesTXTs[7] = "cabe�a, olho esquerdo, olho direito, nariz, boca, tronco, bra�o esquerdo ";
		partesTXTs[8] = "cabe�a, olho esquerdo, olho direito, nariz, boca, tronco, bra�o esquerdo, bra�o direito ";
		partesTXTs[9] = "cabe�a, olho esquerdo, olho direito, nariz, boca, tronco, bra�o esquerdo, bra�o direito, perna esquerda ";
		partesTXTs[10] = "cabe�a, olho esquerdo, olho direito, nariz, boca, tronco, bra�o esquerdo, bra�o direito, perna esquerda, perna direita";
		
		System.out.println(contexto + " BONECO --> " + partesTXTs[partes]);
	}
	

}
