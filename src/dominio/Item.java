	package dominio;

public class Item extends ObjetoDominioImpl{
	
	private boolean[] posicoesDescobertas;
	private String palavraArriscada = null;
	private Palavra palavra;
	
	protected static Item criar(int id, Palavra palavra)
	{
		return new Item(id, palavra);
	}
	
	public static Item reconstituir(long id, Palavra palavra, int[] posicoesDescobertas, String palavraArriscada)
	{
		return new Item(id, palavra, posicoesDescobertas, palavraArriscada);
	}
	
	public Item(long id, Palavra palavra)
	{
		super(id);
		this.palavra = palavra;
		this.posicoesDescobertas = new boolean[palavra.getTamanho()];
		
		for(int i = 0; i != this.posicoesDescobertas.length; i++)
			this.posicoesDescobertas[i] = false;
	}
	
	public Item(long id, Palavra palavra, int[] posicoesDescobertas, String palavraArriscada)
	{
		super(id);
		this.palavra = palavra;
		this.palavraArriscada = palavraArriscada;

		for(int i = 0; i < posicoesDescobertas.length; i++)
			this.posicoesDescobertas[i] = (posicoesDescobertas[i] == 1) ? true : false;
	}
	
	public Palavra getPalavra()
	{
		return this.palavra;
	}
	
	public Letra[] getLetrasDescobertas()
	{
		int i = 0;
		for(int j = 0; j != this.palavra.getTamanho(); j++)
			if(this.posicoesDescobertas[j] == true)
				i++;
		
		Letra[] letrasDescobertas = new Letra[i];

		i = 0;
		for(int j = 0; j != palavra.getTamanho(); j++)
			if(this.posicoesDescobertas[j] == true)
			{
				letrasDescobertas[i] = this.palavra.getLetra(j);
				i++;
			}
		
		return letrasDescobertas;
	}
	
	public Letra[] getLetrasEncobertas()
	{
		int i = 0;
		for(int j = 0; j != this.palavra.getTamanho(); j++)
			if(this.posicoesDescobertas[j] == false)
				i++;
		
		Letra[] letrasEncobertas = new Letra[i];

		i = 0;
		for(int j = 0; j != this.palavra.getTamanho(); j++)
			if(this.posicoesDescobertas[j] == false)
			{
				letrasEncobertas[i] = palavra.getLetra(j);
				i++;
			}
		
		return letrasEncobertas;
	}
	
	public int qtdeLetrasEncobertas()
	{
		int i = 0;
		
		for(int j = 0; j != this.posicoesDescobertas.length; j++)
			if(this.posicoesDescobertas[j] == false)
				i++;
		
		return i;	
	}
	
	public int calcularPontosLetrasEncobertas(int valorPorLetraEncoberta)
	{
		int i = 0;
		
		for(int j = 0; j != this.posicoesDescobertas.length; j++)
			if(this.posicoesDescobertas[j] == false)
				i++;
		
		return i * valorPorLetraEncoberta;
	}
	
	public boolean descobriu() {
		
		/*
		 * N�os sei se � a melhor forma de verificar
		 * se as letras encobertas est�o encobertas de fato.
		 * Vide olhar documentacao.
		 */
		if((this.acertou()) || (this.getLetrasEncobertas().length == 0))
			return true;

		return false;
	}
	
	public void exibir()
	{
		this.palavra.exibir();
	}
	
	boolean tentar(char codigo)
	{
		boolean cond = false;
		
		for(int i = 0; i != this.palavra.getTamanho(); i++)
			if(this.palavra.getLetra(i).getCodigo() == codigo)
			{
				this.posicoesDescobertas[i] = true;
				cond = true;
			}
		
		return cond;
	}
	
	void arriscar(String palavra)
	{
		this.palavraArriscada = palavra;
		
		if(acertou())
			for(int i = 0; i != this.posicoesDescobertas.length; i++)
				this.posicoesDescobertas[i] = true;
	}
	
	public String getPalavraArriscada()
	{
		return this.palavraArriscada;
	}
	
	public boolean arriscou()
	{
		return this.palavraArriscada != null;
	}
	
	public boolean acertou()
	{
		return this.palavra.comparar(palavraArriscada);
	}
}
