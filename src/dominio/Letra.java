package dominio;

public abstract class Letra
{
	private char codigo;
	
	protected Letra(char codigo)
	{
		this.codigo = codigo;
	}
	public char getCodigo()
	{
		return this.codigo;
	}
	
	/**
	 * Exibe a letra
	 */
	public abstract void exibir();

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Letra))
			return false;
		Letra outra = (Letra) o;
		return this.codigo == outra.codigo && this.getClass().equals(outra.getClass());
	}

	@Override
	public int hashCode()
	{
		return this.codigo+this.getClass().hashCode(); 
	}
	
	public final String toString()
	{
		return "" + this.getCodigo();
	}
}