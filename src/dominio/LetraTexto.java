package dominio;

public class LetraTexto extends Letra
{
	public LetraTexto(char codigo)
	{
		super(codigo);
	}
	
	public void exibir()
	{
		System.out.print(this.getCodigo());
	}
}
