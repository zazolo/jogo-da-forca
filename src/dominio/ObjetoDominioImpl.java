package dominio;

public abstract class ObjetoDominioImpl implements ObjetoDominio
{
	private long id = 0;
	
	@Override
	public long getId()
	{
		return this.id;
	}

	public ObjetoDominioImpl(long id)
	{
		this.id = id;
	}
}
