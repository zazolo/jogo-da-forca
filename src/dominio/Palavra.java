package dominio;

import java.util.ArrayList;
import java.util.List;

public class Palavra extends ObjetoDominioImpl
{
	private List<Letra> letras = new ArrayList<Letra>();
	private List<Letra> letrasEncobertas = new ArrayList<Letra>();
	private Tema tema ;
	private static LetraFactory letraFactory;
	
	public static void setLetraFactory(LetraFactory factory)
	{
		letraFactory = factory;
	}
	
	public static LetraFactory getLetraFactory()
	{
		return letraFactory;
	}
	
	public static Palavra criar(long id, String palavra, Tema tema)
	{
		return new Palavra(id, palavra, tema);
	}
	
	public static Palavra reconstituir(long id, String palavra, Tema tema)
	{
		return new Palavra (id, palavra, tema);
	}
	
	private Palavra(long id, String palavra, Tema tema)
	{
		super(id);
		this.tema = tema;
		for(int i = 0; i < palavra.length(); i++)
			this.letras.add(letraFactory.getLetra(palavra.charAt(i)));

		for(int j = 0; j < this.letras.size(); j++) {
			this.letrasEncobertas.add(this.letras.get(j));
		}

		
	}
	
	public Letra[] getLetras()
	{
		Letra[] arrOutput = {};
		for(int i = 0; i < this.letras.size(); i++)
			arrOutput[i] = this.letras.get(i);
		return arrOutput;
	}
	
	/**
	 * Retorna uma LETRA com de acordo com a posicao atual da 
	 * palavra.
	 * 
	 * @param posicao
	 */
	public Letra getLetra(int posicao)
	{
		return this.letras.get(posicao);
	}
	
	public void exibir()
	{
		for(int i = 0; i != this.letras.size(); i++)
			System.out.print(this.letras.get(i).getCodigo());
	}
	
	/**
	 * Exibe a palavra atual em seu estado atual.
	 * @param posicoes
	 */
	public void exibir(boolean[] posicoes)
	{
		for(int i = 0; i != this.letras.size(); i++)
			if(posicoes[i] == true)
				this.letras.get(i).exibir();
	}
	
	/**
	 * Tenta a palavra de acordo com o codigo
	 * @param codigo
	 */
	public int tentar(char codigo)
	{
		int cond = 0;
		
		for(int i = 0; i != this.letras.size(); i++)
			if(this.letrasEncobertas.get(i).getCodigo() == codigo)
			{
				this.letrasEncobertas.set(i, null);
				cond = 1;
			}
		return cond;
	}
	
	/**
	 * Retorna o tema atual
	 * @return Tema
	 */
	public Tema getTema()
	{
		return this.tema;
	}
	
	/**
	 * Compara a String recebida com a palavra;
	 * @param palavra
	 * @return String
	 */
	public boolean comparar(String palavra)
	{
		if(palavra.compareToIgnoreCase(this.toString()) == 0)
			return true;
		else
			return false; 
	}
	
	/**
	 * Retorna o tamanho da Palavra;
	 * @return INT
	 * @throws Exception 
	 */
	public int getTamanho()
	{
		return this.letras.size();
	}
	
	/**
	 * Retorna a palavra em String;
	 */
	public String toString()
	{
		String str = "";
		for(int i = 0; i < this.letras.size(); i++)
			str += this.letras.get(i).getCodigo();
		
		return str;
	}
}
