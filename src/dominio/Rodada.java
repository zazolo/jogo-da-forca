package dominio;

public class Rodada extends ObjetoDominioImpl
{
	private static int maxPalavras = 3;
	private static int maxErros = 10;
	private static int pontosQuandoDescobreAsPalavras = 100;
	private static int pontosPorLetraEncoberta = 3;
	private static BonecoFactory bonecoFactory;
	private Item[] itens = new Item[maxPalavras];
	private Letra[] erradas = new Letra[maxErros];
	private Jogador jogador;
	private Boneco boneco;
	
	public static int getMaxPalavras()
	{
		return maxPalavras;
	}
	
	public static void setMaxPalavras(int max)
	{
		maxPalavras = max;
	}
	
	public static int getMaxErros()
	{
		return maxErros;
	}
	
	public static void setMaxErros(int max)
	{
		maxErros = max;
	}
	
	public static int getPontosQuandoDescobreTodasAsPalavras()
	{
		return pontosQuandoDescobreAsPalavras;
	}
	
	public static void setPontosQuandoDescobreTodasAsPalavras(int pontos)
	{
		pontosQuandoDescobreAsPalavras = pontos;
	}
	
	public static int getPontosPorLetraEncoberta()
	{
		return pontosPorLetraEncoberta;
	}
	
	public static void setPontosPorLetraEncoberta(int pontos)
	{
		pontosPorLetraEncoberta = pontos;
	}
	
	public static void setBonecoFactory(BonecoFactory Factory)
	{
		bonecoFactory = Factory;
	}
	
	public static BonecoFactory getBonecoFactory()
	{
		return bonecoFactory;
	}
	
	public static Rodada criar(long id , Palavra[] palavras , Jogador jogador) throws Exception
	{
		if(bonecoFactory == null)
			throw new Exception("BonecoFactory n�o foi instanciado!");
		return new Rodada(id, palavras, jogador);
	}
	
	public static Rodada rconstituir(long id , Item[] itens , Letra[] erradas , Jogador jogador) throws Exception
	{
		if(bonecoFactory == null)
			throw new Exception("BonecoFactory n�o foi instanciado!");
		return new Rodada(id, itens, erradas, jogador);
	}
	
	private Rodada(long id , Palavra[] palavras , Jogador jogador)
	{
		super(id);
		for(int i = 0; i < maxPalavras; i++)
			itens[i] = new Item(i, palavras[i]);
		this.jogador = jogador;
		boneco = bonecoFactory.getBoneco();
	}
	
	private Rodada(long id , Item[] itens , Letra[] erradas , Jogador jogador)
	{
		super(id);
		this.itens = itens;
		this.erradas = erradas;
		this.jogador = jogador;
		boneco = bonecoFactory.getBoneco();
	}
	
	public Jogador getJogador()
	{
		return jogador;
	}
	
	public Tema getTema()
	{
		return this.itens[0].getPalavra().getTema();
	}
	
	public Palavra[] getPalavras()
	{
		Palavra[] palavras = new Palavra[maxPalavras];
		for(int i = 0; i != this.itens.length; i++)
			palavras[i] = this.itens[i].getPalavra();
		
		return palavras;
	}
	
	public int getNumPalavras()
	{
		int i = 0;
		for(int j = 0; j != itens.length; j++)
			if(this.itens[j].getPalavra() != null)
				i++;
		
		return i;
	}
	
	public void tentar(char codigo )
	{
		boolean cond = false;
		
		for(int i = 0; i != this.getNumPalavras(); i++)
			if(this.itens[i].tentar(codigo))
				cond = true;
		
		if(cond == false)
		{
			LetraFactory letraFactory = Palavra.getLetraFactory();
			this.erradas[this.getQtdeErros()] = letraFactory.getLetra(codigo);
		}
	}
	
	//VERIFICAR!!!
	public void arriscar(String[] palavras )
	{
		for(int i = 0; i != palavras.length; i++)
			this.itens[i].arriscar(palavras[i]);
	}
	
	//VERIFICAR!!!
	public void exibirItens()
	{
		for(int i = 0; i != this.itens.length; i++)
			this.itens[i].exibir();
	}
	
	public void exibirBoneco(Object contexto)
	{
		this.boneco.exibir(contexto, this.getQtdeErros());
	}
	
	public void exibirPalavras()
	{
		for(int i = 0; i != this.itens.length; i++)
			this.itens[i].getPalavra().exibir();
	}
	
	public Letra[] getTentativas()
	{
		int i = 0;
		Letra[] tentativas = new Letra[this.getQtdeTentativas()];
		
		Letra[] vetorAux = new Letra[this.getQtdeErros()];
		vetorAux = this.getErradas();
		for(int j = 0; j != vetorAux.length; j++)
		{
			tentativas[i] = vetorAux[j];
			i++;
		}
		
		vetorAux = new Letra[this.getQtdeAcertos()];
		vetorAux = this.getCertas();
		for(int j = 0; j != vetorAux.length; j++)
		{
			tentativas[i] = vetorAux[j];
			i++;
		}
		
		return tentativas;
	}
	
	//POSS�VEL DISOTIMIZA��O, FAVOR VERIFICAR
	public Letra[] getCertas()
	{
		int i = 0;
		for(int j = 0; j >= this.itens.length; j++)
			for(int k = 0; k != itens[j].getLetrasDescobertas().length; k++)
				i += itens[j].getLetrasDescobertas().length;

		Letra[] certas = new Letra[i];
		i = 0;
		
		for(int j = 0; j >= this.itens.length; j++)
		{
			Letra[] vetorAux = itens[j].getLetrasDescobertas();
			for(int k = 0; k != vetorAux.length; k++)
			{
				certas[i] = vetorAux[k];
				i++;
			}
		}
		
		return certas;
	}
	
	public Letra[] getErradas()
	{
		return this.erradas;
	}
	
	public int calcularPontos()
	{
		int i = 0;
		
		for(int j = 0; j != itens.length; j++)
			if(itens[j].descobriu())
				i += (itens[j].getPalavra().getTamanho() * pontosPorLetraEncoberta) + pontosQuandoDescobreAsPalavras;
			else
				i += itens[j].getLetrasEncobertas().length * pontosPorLetraEncoberta;
		
		return i;
	}
	
	public boolean encerrou()
	{
		if(erradas[maxErros - 1] != null)
			return true;
		else if(this.arriscou())
			return true;
		else if(this.descobriu())
			return true;
		
		return false;
	}
	
	public boolean descobriu()
	{
		for(int i = 0; i != itens.length; i++)
			if(!itens[i].descobriu())
				return false;
		
		return true;
	}
	
	public boolean arriscou()
	{
		for(int i = 0; i != maxPalavras; i++)
			if(itens[i].getPalavraArriscada() != null)
				return true;
		
		return false;
	}
	
	public int getQtdeTentativaRestantes()
	{
		return maxErros - erradas.length;
	}
	
	public int getQtdeErros()
	{
		return erradas.length;
	}
	
	public int getQtdeAcertos()
	{
		int i = 0;
		
		for(int j = 0; j != maxPalavras; j++)
		{
			Letra[] letrasDescobertas = itens[j].getLetrasDescobertas();
			i += letrasDescobertas.length;
		}
		
		return i;
	}
	
	public int getQtdeTentativas()
	{
		int i = 0;
		while(erradas[i] != null)
			i++;
		
		return i + getQtdeAcertos();
	}
}
