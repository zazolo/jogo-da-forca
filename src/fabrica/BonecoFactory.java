package fabrica;

import dominio.Boneco;

public interface BonecoFactory {
	public Boneco getBoneco();
}
