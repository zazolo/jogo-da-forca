package fabrica;

import dominio.Boneco;
import dominio.BonecoTexto;

public class BonecoTextoFactory implements BonecoFactory {

	private static BonecoTextoFactory soleInstance = new BonecoTextoFactory();
	
	public static BonecoTextoFactory getSoleInstance() {
		return soleInstance;
	}
	
	private BonecoTextoFactory() { }
	
	@Override
	public Boneco getBoneco() {
		return BonecoTexto.getSoleInstance();
	}

}
