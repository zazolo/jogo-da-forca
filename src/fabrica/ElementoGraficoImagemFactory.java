package fabrica;

import dominio.Boneco;
import dominio.Letra;

public class ElementoGraficoImagemFactory implements ElementoGraficoFactory
{
	private static ElementoGraficoImagemFactory soleInstance;
	private BonecoTextoFactory btf;
	private LetraTextoFactory ltf;
			
	public static ElementoGraficoImagemFactory getSoleInstance()
	{
		return soleInstance;
	}
	
	private ElementoGraficoImagemFactory()
	{
		btf = BonecoTextoFactory.getSoleInstance();
		ltf = LetraTextoFactory.getSoleInstance();
	}
	
	public Boneco getBoneco()
	{
		return null;
	}

	@Override
	public Letra getLetra(char codigo)
	{
		return null;
	}

	@Override
	public Letra getLetraEncoberta()
	{
		return null;
	}

}
