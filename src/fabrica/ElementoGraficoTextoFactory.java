package fabrica;

import dominio.Boneco;
import dominio.Letra;

public class ElementoGraficoTextoFactory implements ElementoGraficoFactory {

	private static ElementoGraficoTextoFactory soleInstance = new ElementoGraficoTextoFactory();
	private BonecoTextoFactory btf;
	private LetraTextoFactory ltf;
			
	
	private ElementoGraficoTextoFactory() {
		btf = BonecoTextoFactory.getSoleInstance();
		ltf = LetraTextoFactory.getSoleInstance();
	}
	@Override
	public Boneco getBoneco() {
		return btf.getBoneco();
	}

	@Override
	public Letra getLetra(char codigo) {
		return ltf.getLetra(codigo);
	}

	@Override
	public Letra getLetraEncoberta() {
		return ltf.getLetraEncoberta();
	}

	public static ElementoGraficoTextoFactory getSoleInstance() {
		return soleInstance;
	}
}
