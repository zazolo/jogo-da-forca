package fabrica;

import repositorio.Repository;

public abstract class EntityFactory {

	private Repository repositorio;
	
	protected EntityFactory(Repository repositorio) {
		this.repositorio = repositorio;
	}
	
	protected Repository getRepository() {
		return this.repositorio;
	}
	
	protected long getProximoId() {
		return this.repositorio.getProximoId();
	}
	
	

}
