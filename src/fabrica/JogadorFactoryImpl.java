package fabrica;

import dominio.Jogador;
import repositorio.JogadorRepository;
import repositorio.Repository;

public class JogadorFactoryImpl extends EntityFactory implements JogadorFactory {

	private static JogadorFactoryImpl soleInstance;
	
	public static void createSoleInstance(JogadorRepository repositorio) {
		soleInstance = new JogadorFactoryImpl(repositorio);
	}
	
	public static JogadorFactoryImpl getSoleInstance() {
		return soleInstance;
	}
	
	protected JogadorFactoryImpl(Repository repositorio) {
		super(repositorio);
	}
	
	private JogadorRepository getJogadorRepository() {
		return (JogadorRepository) this.getRepository();
	}

	@Override
	public Jogador getJogador(String nome) {
		JogadorRepository jogadorRepo = this.getJogadorRepository();
		return Jogador.criar(jogadorRepo.getProximoId(), nome);
	}

}
