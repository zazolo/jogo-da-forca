package fabrica;

import dominio.Letra;
import dominio.LetraFactory;

public abstract class LetraFactoryImpl implements LetraFactory {
	
	Object algumaLetra;
	
	public final Letra getLetra(char codigo) {
		return this.criarLetra(codigo);
	}
	public final Letra getLetraEncoberta() {
		return (Letra) this.algumaLetra;
		
	}
	protected abstract Letra criarLetra(char codigo);
}
