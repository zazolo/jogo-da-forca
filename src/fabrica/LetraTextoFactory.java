package fabrica;

import java.util.ArrayList;
import java.util.List;

import dominio.Letra;
import dominio.LetraTexto;

public class LetraTextoFactory extends LetraFactoryImpl{
	
	private static LetraTextoFactory soleInstance = new LetraTextoFactory();
	
	private List<Letra> letras = new ArrayList<Letra>();
	
	private LetraTextoFactory() {
		this.algumaLetra = new LetraTexto('*');
	}
	protected Letra criarLetra(char codigo) {
		Letra nLetra = new LetraTexto(codigo);

		for(int i = 0; i < letras.size(); i++) {
			if(letras.get(i).equals(nLetra)) {
				return letras.get(i);
			}
		}
		
		letras.add(nLetra);
		
		return this.criarLetra(codigo);
	}
	
	public static LetraTextoFactory getSoleInstance() {
		return soleInstance;
	}
	
	
}
