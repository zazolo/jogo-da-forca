package fabrica;

import dominio.Palavra;
import dominio.Tema;
import repositorio.PalavraRepository;

public class PalavraFactoryImpl extends EntityFactory implements PalavraFactory {

	private static PalavraFactoryImpl soleInstance;
	
	public static void createSoleInstance(PalavraRepository repositorio) {
		soleInstance = new PalavraFactoryImpl(repositorio);
	}
	
	public static PalavraFactoryImpl getSoleInstance() {
		return soleInstance;
	}
	
	public PalavraFactoryImpl(PalavraRepository repositorio) {
		super(repositorio);
	}
	
	private PalavraRepository getPalavraRepository() {
		return (PalavraRepository) this.getRepository();
	}
	@Override
	public Palavra getPalavra(String palavra, Tema tema) {
		PalavraRepository palavraRepo = this.getPalavraRepository();
		return Palavra.criar(palavraRepo.getProximoId(), palavra, tema);
	}

}
