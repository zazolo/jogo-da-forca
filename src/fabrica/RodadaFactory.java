package fabrica;

import dominio.*;
public interface RodadaFactory {
	public Rodada getRodada(Jogador jogador);
}
