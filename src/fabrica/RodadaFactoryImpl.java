package fabrica;

import repositorio.PalavraRepository;
import repositorio.RodadaRepository;
import repositorio.TemaRepository;

public abstract class RodadaFactoryImpl extends EntityFactory implements RodadaFactory {

	private RodadaRepository rodadaRepo;
	private TemaRepository temaRepo;
	private PalavraRepository palavraRepo;
	protected RodadaFactoryImpl(RodadaRepository repository, TemaRepository temaRepository, PalavraRepository palavraRepository) {
		super(repository);
		this.rodadaRepo = repository;
		this.temaRepo = temaRepository;
		this.palavraRepo = palavraRepository;
	}
	
	protected RodadaRepository getRodadaRepository() {
		return this.rodadaRepo;
	}
	
	protected TemaRepository getTemaRepository() {
		return this.temaRepo;
	}
	
	protected PalavraRepository getPalavraRepository() {
		return this.palavraRepo;
	}

}
