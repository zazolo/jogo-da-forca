package fabrica;

import repositorio.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import dominio.*;

public class RodadaSorteioFactory extends RodadaFactoryImpl{
	
	private static RodadaSorteioFactory soleInstance;
	
	public static void createSoleInstance(RodadaRepository repository, TemaRepository temaRepository, PalavraRepository palavraRepository) {
		soleInstance = new RodadaSorteioFactory(repository, temaRepository, palavraRepository);
	}
	
	public static RodadaSorteioFactory getSoleInstance() {
		return soleInstance;
		
	}
	
	private RodadaSorteioFactory(RodadaRepository repository, TemaRepository temaRepository, PalavraRepository palavraRepository) {
		super(repository, temaRepository, palavraRepository);
	}
	
	public Rodada getRodada(Jogador jogador) {
		
			Random randomizer = new Random();
			
			long idProximaRodada = this.getRodadaRepository().getProximoId();
			int quantidadePalavrasSorteadas = randomizer.nextInt(3); //-->Quantidade de palavras que a Rodada vai possuir;
			
			//int posicaoTemaEscolhido = randomizer.nextInt(this.getTemaRepository().getTodos().length + 1); //-->futura posicao que ser� utilizada abaixo;
			int posicaoTemaEscolhido = this.getTemaRepository().getTodos().length - 1;
			Tema temaEscolhido = this.getTemaRepository().getTodos()[posicaoTemaEscolhido]; //-->tema escolhido;
			

			List<Palavra> palavrasDoTema = Arrays.asList(this.getPalavraRepository().getPorTema(temaEscolhido));
			List<Palavra> palavrasEscolhidas = new ArrayList<Palavra>();
			int random = 0;
			for(int i = 0; i < quantidadePalavrasSorteadas; i++) { //--Sorteia as palavras do tema e as p�e no array;
				random = randomizer.nextInt(palavrasDoTema.size());
				if(random < 0) {
					random = 0;
				}
				palavrasEscolhidas.add(palavrasDoTema.get(random));
			}

			int si = palavrasEscolhidas.size() -1;
			if(si < 0) {
				si=0;
			}
			Palavra[] pFinal = new Palavra[palavrasEscolhidas.size()]; 
			try {
				System.out.println("Criando a rodada...\nID:" +idProximaRodada+"\nPalavrasEscolhidas: " + palavrasEscolhidas.size()  );
				return Rodada.criar(idProximaRodada, palavrasEscolhidas.toArray(pFinal), jogador);	 
			} catch (Exception e) {
				System.out.println("Ocorreu um erro ao tentar criar a rodada no m�todo RodadaSorteioFactory m�todo getRodada(?); RETORNANDO NULL...");
				return null;
			}
		
	}
}
