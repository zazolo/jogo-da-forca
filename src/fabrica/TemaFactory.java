package fabrica;

import dominio.Tema;

public interface TemaFactory {
	public Tema getTema(String nome);
}
