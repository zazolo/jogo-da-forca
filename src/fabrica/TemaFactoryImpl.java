package fabrica;

import dominio.Tema;
import repositorio.TemaRepository;

public class TemaFactoryImpl extends EntityFactory implements TemaFactory  {

	
	private static TemaFactoryImpl soleInstance;
	
	public static void createSoleInstance(TemaRepository repositorio) {
		soleInstance = new TemaFactoryImpl(repositorio);
	}
	
	public static TemaFactoryImpl getSoleInstance() {
		return soleInstance;
	}
	
	private TemaFactoryImpl(TemaRepository repositorio) {
		super(repositorio);
	}
	
	private TemaRepository getTemaRepository() {
		return (TemaRepository) this.getRepository();
	}

	@Override
	public Tema getTema(String nome) {
		
		try {
			TemaRepository temaRepo = this.getTemaRepository();
			Tema[] temas = temaRepo.getTodos();
			int i = 0;
			for(i = 0; i < temas.length; i++) {
				if(temas[i].getNome().equalsIgnoreCase(nome)) {
					return temas[i];
				}
			}
			temaRepo.inserir(Tema.criar(i, nome));	
			return this.getTema(nome);
		} catch (Exception e) {
			System.out.println("Falha ao obter o tema (getTema) na FactoryImpl.");
		}
		
		return null;
	}

}
