import aplicacao.Aplicacao;
import dominio.Jogador;
import dominio.Palavra;
import dominio.Rodada;
import dominio.Tema;
import repositorio.RepositoryException;

public class play {

	public static void main(String[] args) {
		Aplicacao jogo = Aplicacao.getSoleInstance();
		
		jogo.setup();
		
	
		try {
			
			/*
			 * Cria o banco e armazena dados
			 */
			Tema tema = jogo.getTemaFactory().getTema("animais da turma");

			jogo.getRepositoryFactory().getRepoTemaRepository().inserir(tema);
			
			Palavra.setLetraFactory(jogo.getElementoGraficoFactory());
			Palavra palavra_1 = jogo.getPalavraFactory().getPalavra("ciro", tema);
			Palavra palavra_2 = jogo.getPalavraFactory().getPalavra("tiago", tema);
			Palavra palavra_3 = jogo.getPalavraFactory().getPalavra("luidi", tema);
			Palavra palavra_4 = jogo.getPalavraFactory().getPalavra("maycondouglas", tema);
			Palavra palavra_5 = jogo.getPalavraFactory().getPalavra("lucas", tema);
			Palavra palavra_6 = jogo.getPalavraFactory().getPalavra("leo", tema);
			
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_1);
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_2);
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_3);
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_4);
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_5);
			jogo.getRepositoryFactory().getPalavraRepository().inserir(palavra_6);
			
			
			/*
			 * Inicia a partida;
			 */
			Jogador jogador = jogo.getJogadorFactory().getJogador("Pedro");
			
			Rodada rodada = jogo.getRodadaFactory().getRodada(jogador);
			
			rodada.tentar('a');
			rodada.tentar('n');
			rodada.tentar('i');
			rodada.tentar('m');
			rodada.tentar('s');
			rodada.tentar('d');
			rodada.tentar('t');
			rodada.tentar('u');
			rodada.tentar('r');
			
			
			
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
