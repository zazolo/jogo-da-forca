package repositorio;

public class BDRRepositoryFactory implements RepositoryFactory
{
	private static BDRRepositoryFactory soleInstance;
	
	 public static BDRRepositoryFactory getSoleInstance()
	{
		return soleInstance;
	}
	
	private BDRRepositoryFactory()
	{
		
	}

	public PalavraRepository getPalavraRepository()
	{
		return null;
	}

	public TemaRepository getRepoTemaRepository()
	{
		return null;
	}

	public RodadaRepository getRodadaRepository()
	{
		return null;
	}

	public JogadorRepository getJogadorRepository()
	{
		return null;
	}

}
