package repositorio;

import java.util.ArrayList;
import java.util.List;

import dominio.Jogador;

public class MemoriaJogadorRepository implements JogadorRepository {

	private static MemoriaJogadorRepository soleInstance = new MemoriaJogadorRepository();
	
	private List<Jogador> BDJogadores = new ArrayList<Jogador>();
	
	private MemoriaJogadorRepository() { }
	
	@Override
	public long getProximoId() {
		return this.BDJogadores.size() + 1;
	}

	@Override
	public Jogador getPorId(long id) {
		return (Jogador) this.BDJogadores.get((int) id);
	}

	@Override
	public Jogador getPorNome(String nome) {
		for(int i = 0; i < this.BDJogadores.size(); i++) {
			if(this.BDJogadores.get(i).getNome() == nome) {
				return this.BDJogadores.get(i);
			}
		}
		return null;
	}

	@Override
	public void inserir(Jogador jogador) throws RepositoryException {
		this.BDJogadores.add(jogador);
	}

	@Override
	public void atualizar(Jogador jogador) throws RepositoryException {
		System.out.println("Tema atualizado no repositório: " + jogador.getNome());
		for(int i = 0; i < this.BDJogadores.size(); i++) {
			if(this.BDJogadores.get(i).getId() == jogador.getId()) {
				this.BDJogadores.set(i, jogador);
			}
		}
	}

	@Override
	public void remover(Jogador jogador) throws RepositoryException {
		for(int i = 0; i < this.BDJogadores.size(); i++) {
			if(this.BDJogadores.get(i).getId() == jogador.getId()) {
				System.out.println("Jogador removido do repositório: " + this.BDJogadores.get(i).getNome());
				this.BDJogadores.remove(i);
			}
		}
	}
	
	public static MemoriaJogadorRepository getSoleInstance() {
		return soleInstance;
	}

}
