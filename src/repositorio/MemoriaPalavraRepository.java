package repositorio;

import java.util.ArrayList;
import java.util.List;

import dominio.Palavra;
import dominio.Tema;

public class MemoriaPalavraRepository implements PalavraRepository {

	private static MemoriaPalavraRepository soleInstance = new MemoriaPalavraRepository();
	
	private MemoriaPalavraRepository() { }
	
	private List<Palavra> BDPalavras = new ArrayList<Palavra>();
	
	@Override
	public Palavra getPorId(long id) {
		return (Palavra) this.BDPalavras.get((int) id);
	}

	@Override
	public Palavra[] getPorTema(Tema tema) {
		List<Palavra> pCast = new ArrayList<Palavra>();
		
		System.out.println("Quantidade no BD: " + this.BDPalavras.size());
		for(int i = 0; i < this.BDPalavras.size(); i++) {
			
			if(this.BDPalavras.get(i).getTema().getNome().equalsIgnoreCase(tema.getNome())) {
				pCast.add(this.BDPalavras.get(i));
				
			}
		}
		Palavra[] palavras = new Palavra[pCast.size()];
		return pCast.toArray(palavras);
	}

	@Override
	public Palavra[] getTodas() {
		Palavra[] palavras = new Palavra[] {};
		for(int i = 0; i < this.BDPalavras.size(); i++) {
			palavras[i] = this.BDPalavras.get(i);
		}
		return palavras;
	}

	@Override
	public Palavra getPalavra(String palavra) {
		System.out.println("Buscando pela palavra repositorio: " + palavra);
		for(int i = 0; i < this.BDPalavras.size(); i++) {
			if(this.BDPalavras.get(i).comparar(palavra)) {
				System.out.println("Palavra encontrada! Retornando o objeto: " + this.BDPalavras.get(i).getId());
				return this.BDPalavras.get(i);
			}
		}
		return null;
	}

	@Override
	public void inserir(Palavra palavra) throws RepositoryException {
		System.out.println("Palavra inserida no repositorio: " + palavra.toString());
		this.BDPalavras.add(palavra);
	}

	@Override
	public void atualizar(Palavra palavra) throws RepositoryException {
		System.out.println("Palavra atualizada no repositório: " + palavra.toString());
		for(int i = 0; i < this.BDPalavras.size(); i++) {
			if(this.BDPalavras.get(i).getId() == palavra.getId()) {
				this.BDPalavras.set(i, palavra);
			}
		}
	}

	@Override
	public void remover(Palavra palavra) throws RepositoryException {
		for(int i = 0; i < this.BDPalavras.size(); i++) {
			if(this.BDPalavras.get(i).getId() == palavra.getId()) {
				System.out.println("Palavra removido do repositório: " + this.BDPalavras.get(i).toString());
				this.BDPalavras.remove(i);
			}
		}		

	}
	
	public static MemoriaPalavraRepository getSoleInstance() {
		return soleInstance;
	}

	@Override
	public long getProximoId() {
		return this.BDPalavras.size() + 1;
	}
	
	

}
