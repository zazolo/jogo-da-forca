package repositorio;

public class MemoriaRepositoryFactory implements RepositoryFactory {

	private static MemoriaRepositoryFactory soleInstance = new MemoriaRepositoryFactory();

	private MemoriaRepositoryFactory() { }
	
	@Override
	public PalavraRepository getPalavraRepository() {
		return MemoriaPalavraRepository.getSoleInstance();
	}

	@Override
	public TemaRepository getRepoTemaRepository() {
		return MemoriaTemaRepository.getSoleInstance();
	}

	@Override
	public RodadaRepository getRodadaRepository() {
		return MemoriaRodadaRepository.getSoleInstance();
	}

	@Override
	public JogadorRepository getJogadorRepository() {
		return MemoriaJogadorRepository.getSoleInstance();
	}

	public static MemoriaRepositoryFactory getSoleInstance() {
		return soleInstance;
	}
}
