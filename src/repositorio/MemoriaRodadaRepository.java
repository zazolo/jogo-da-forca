package repositorio;

import java.util.ArrayList;
import java.util.List;

import dominio.Jogador;
import dominio.Rodada;

public class MemoriaRodadaRepository implements RodadaRepository {
	
	private static MemoriaRodadaRepository soleInstance = new MemoriaRodadaRepository();
	
	private MemoriaRodadaRepository() { }

	private List<Rodada> BDRodada = new ArrayList<Rodada>();
	@Override
	public Rodada getPorId(long id) {
		return (Rodada) this.BDRodada.get((int) id);
	}

	@Override
	public Rodada[] getPorJogador(Jogador jogador) {
		List<Rodada> pCast = new ArrayList<Rodada>();
		
		System.out.println("Quantidade no BD: " + this.BDRodada.size());
		for(int i = 0; i < this.BDRodada.size(); i++) {
			
			if(this.BDRodada.get(i).getJogador().getNome().equalsIgnoreCase(jogador.getNome())) {
				pCast.add(this.BDRodada.get(i));
				
			}
		}
		if(pCast.isEmpty()) { return null;}
		Rodada[] palavras = new Rodada[pCast.size()];
		return pCast.toArray(palavras);
	}

	@Override
	public void inserir(Rodada rodada) throws RepositoryException {
		System.out.println("Rodada inserida no repositorio: " + rodada.toString());
		this.BDRodada.add(rodada);
	}

	@Override
	public void atualizar(Rodada rodada) throws RepositoryException {
		System.out.println("Rodada atualizada no repositório: " + rodada.toString());
		for(int i = 0; i < this.BDRodada.size(); i++) {
			if(this.BDRodada.get(i).getId() == rodada.getId()) {
				this.BDRodada.set(i, rodada);
			}
		}
	}

	@Override
	public void remover(Rodada rodada) throws RepositoryException {
		for(int i = 0; i < this.BDRodada.size(); i++) {
			if(this.BDRodada.get(i).getId() == rodada.getId()) {
				System.out.println("Palavra removido do repositório: " + this.BDRodada.get(i).toString());
				this.BDRodada.remove(i);
			}
		}	
	}
	
	public static MemoriaRodadaRepository getSoleInstance() {
		return soleInstance;
	}

	@Override
	public long getProximoId() {
		return this.BDRodada.size();
	}

}
