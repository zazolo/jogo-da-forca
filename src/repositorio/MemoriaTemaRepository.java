package repositorio;

import java.util.ArrayList;
import java.util.List;

import dominio.Tema;

public class MemoriaTemaRepository implements TemaRepository{

	private static MemoriaTemaRepository soleInstance = new MemoriaTemaRepository();
	
	private List<Tema> BDTemas = new ArrayList<Tema>();
	
	private MemoriaTemaRepository() { }
	
	@Override
	public Tema getPorId(long id) {
		return (Tema) this.BDTemas.get((int) id);
	}

	@Override
	public Tema[] getPorTema(Tema tema) {
		Tema[] temas = new Tema[] {};
		int pos = 0;
		for(int i = 0; i < this.BDTemas.size(); i++) {
			if(this.BDTemas.get(i).getNome() == tema.getNome()) {
				temas[pos] = this.BDTemas.get(i);
				pos++;
			}
		}
		return temas;
	}

	@Override
	public Tema[] getTodos() {
		Tema[] temas = new Tema[this.BDTemas.size()];
		return this.BDTemas.toArray(temas);
	}

	@Override
	public void inserir(Tema tema) throws RepositoryException {
		System.out.println("Um novo tema inserido no reposit�rio: " + tema.getNome());
		this.BDTemas.add(tema);
	}

	@Override
	public void atualizar(Tema tema) throws RepositoryException {
		System.out.println("Tema atualizado no reposit�rio: " + tema.getNome());
		for(int i = 0; i < this.BDTemas.size(); i++) {
			if(this.BDTemas.get(i).getId() == tema.getId()) {
				this.BDTemas.set(i, tema);
			}
		}
	}

	@Override
	public void remover(Tema tema) throws RepositoryException {
		for(int i = 0; i < this.BDTemas.size(); i++) {
			if(this.BDTemas.get(i).getId() == tema.getId()) {
				System.out.println("Tema removido do reposit�rio: " + this.BDTemas.get(i).getNome());
				//-->O banco � em mem�ria, ent�o n�o vejo necessidade em remover as palavras tamb�m, uma vez que ser�o inutilziadas ap�s encerramento do programa;
				this.BDTemas.remove(i);
			}
		}		
	}
	
	public static MemoriaTemaRepository getSoleInstance() {
		return soleInstance;
	}

	@Override
	public long getProximoId() {
		return this.BDTemas.size() + 1;
	}

}
