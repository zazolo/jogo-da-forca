package repositorio;

public interface RepositoryFactory {
	public PalavraRepository getPalavraRepository();
	public TemaRepository getRepoTemaRepository();
	public RodadaRepository getRodadaRepository();
	public JogadorRepository getJogadorRepository();
}
