package repositorio;

import dominio.Tema;

public interface TemaRepository extends Repository{
	public Tema getPorId(long id);
	public Tema[] getPorTema(Tema tema);
	public Tema[] getTodos();
	public void inserir(Tema tema) throws RepositoryException;
	public void atualizar(Tema tema) throws RepositoryException;
	public void remover(Tema tema) throws RepositoryException;
}
