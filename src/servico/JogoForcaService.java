package servico;

import dominio.Jogador;
import dominio.Palavra;
import dominio.Rodada;
import fabrica.RodadaFactory;
import repositorio.PalavraRepository;
import repositorio.RepositoryException;
import repositorio.RodadaRepository;

public class JogoForcaService
{
	private static JogoForcaService soleInstance;
	private PalavraRepository palavraRepository;
	private RodadaRepository rodadaRepository;
	private RodadaFactory rodadaFactory;
	
	public void createSoleInstance(PalavraRepository palavraRepository, RodadaRepository rodadaRepository, RodadaFactory rodadaFactory)
	{
		soleInstance = new JogoForcaService(palavraRepository, rodadaRepository, rodadaFactory);
	}
	
	private static JogoForcaService getSoleInstance()
	{
		return soleInstance;
	}
	
	private JogoForcaService(PalavraRepository palavraRepository, RodadaRepository rodadaRepository, RodadaFactory rodadaFactory)
	{
		this.palavraRepository = palavraRepository;
		this.rodadaRepository = rodadaRepository;
		this.rodadaFactory = rodadaFactory;
	}
	
	public void novaPalavra(Palavra palavra) throws PalavraRepetidaException
	{
		if(this.palavraRepository.getPalavra(palavra.toString()) != null)
			throw new PalavraRepetidaException(palavra);
		
		try
		{
			this.palavraRepository.inserir(palavra);
		}
		catch (RepositoryException e)
		{
			e.printStackTrace();
		}
	}
	
	public Rodada novaRodada(Jogador jogador)
	{
		return this.rodadaFactory.getRodada(jogador);
	}
	
	public void salvarRodada(Rodada rodada) throws RepositoryException
	{
		this.rodadaRepository.inserir(rodada);
	}
}
