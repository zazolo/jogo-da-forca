package servico;

import dominio.Palavra;

public class PalavraRepetidaException extends Exception {
	
	private Palavra palavra;
	
	public PalavraRepetidaException(Palavra palavra) {
		this.palavra = palavra;
		
		/*
		 * Falta implementar a excess�o aqui???
		 */
	}
	
	public Palavra getPalavra() {
		return this.palavra;
	}
	
	
}
